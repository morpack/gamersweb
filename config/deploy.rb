# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'tgnmc.com'
set :repo_url, 'git@bitbucket.org:morpack/gamersweb.git'

set :deploy_to, "/srv/www/tgnmc.com"

set :git_enable_submodules, true
set(:git_enable_submodules, true)

set :deploy_via, :remote_cache

set :log_level, :info
set :pty, true

set :linked_dirs, %w{media}

namespace :deploy do

  desc "Restart application"
  task :restart do
    on roles(:app) do
      sudo 'service', 'nginx', 'restart'
    end
  end
end


namespace :composer do
  desc "Install Composer depedencies"
  task :install do
    on roles(:app) do
      within release_path do
        execute "composer", "install", "--no-dev --no-scripts --quiet"
      end
    end
  end
end

before "deploy:updated", "composer:install"
