<?php
/*
Template Name: Home Page Template
*/
?>

<?php get_header(); ?>


<div class="homebanner">
  <div class="container">
    <div class="row">
      <div class="col-md-18 col-md-offset-0">
        <h1>The Gamers Network</h1>
        <p>One of the minecraft server out there.</p>
           <p class="bs-component">
              <a href="#" class="btn btn-info btn-lg">PLAY NOW</a>
            </p>
      </div>
    </div><!-- end .row-->
  </div> <!-- end .container-->
</div> <!-- end #banner-->

<div class="container">
  <div class="row text-center">

    <!--Section 1-->
    <div class="col-sm-4 about">
      <i class="fa fa-gamepad fa-5x"></i>
      <h3>Games</h3>
      <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum </p>
    </div>

    <!--Section 2-->
    <div class="col-sm-4 about">
      <i class="fa fa-flask fa-5x"></i>
      <h3>Survival</h3>
      <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum </p>
    </div>

    <!--Section 3-->
    <div class="col-sm-4 about">
      <i class="fa fa-signal fa-5x"></i>
      <h3>Reliability</h3>
      <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum </p>
    </div>

  </div>
</div>

<?php get_footer(); ?>
